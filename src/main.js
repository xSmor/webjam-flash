import * as THREE from 'https://cdn.skypack.dev/three@0.133.1';
import { GLTFLoader } from 'https://cdn.skypack.dev/three@0.133.1/examples/jsm/loaders/GLTFLoader.js';
// import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

/*===ThreeJS===*/

//Scene, camera, renderer
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
const renderer = new THREE.WebGLRenderer({
    canvas: document.querySelector('#bg'),
    antialias: true,
});

renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);

if (window.innerWidth < 700) {
    camera.position.set(-8, 52, 50);
} else {
    camera.position.set(-18, 52, 50);
}
camera.rotation.set(-0.05, -0.12, 0);

//Lights
const directionalLightFrontUp = new THREE.DirectionalLight(0xffffff, 6);
// const helperFrontUp = new THREE.DirectionalLightHelper(directionalLightFrontUp, 10);

const directionalLightFrontDown = new THREE.DirectionalLight(0xffffff, 2);
// const helperFrontDown = new THREE.DirectionalLightHelper(directionalLightFrontDown, 10);

const directionalLightBackUp = new THREE.DirectionalLight(0xffffff, 6);
// const helperBackUp = new THREE.DirectionalLightHelper(directionalLightBackUp, 10);

const directionalLightSide = new THREE.DirectionalLight(0xffffff, 5);
// const helperSide = new THREE.DirectionalLightHelper(directionalLightSide, 10);

directionalLightFrontUp.position.set(0, 90, 90);
directionalLightFrontUp.target.position.set(0, 30, 0);

directionalLightFrontDown.position.set(0, 0, 90);
directionalLightFrontDown.target.position.set(0, 60, 0);

directionalLightBackUp.position.set(0, 90, -90);
directionalLightBackUp.target.position.set(0, 30, 0);

directionalLightSide.position.set(-90, 90, 0);
directionalLightSide.target.position.set(0, 50, 0);

const ambientLight = new THREE.AmbientLight(0xffffff, 2);

//Texture
const textureBg = new THREE.TextureLoader().load('utils/misc/bg.jpg');
scene.background = textureBg;

scene.add(
    ambientLight,
    directionalLightFrontUp,
    directionalLightFrontUp.target,
    // helperFrontUp,
    directionalLightFrontDown,
    directionalLightFrontDown.target,
    // helperFrontDown,
    directionalLightBackUp,
    directionalLightBackUp.target,
    // helperBackUp,
    directionalLightSide,
    directionalLightSide.target,
    // helperSide,
);

const loader = new GLTFLoader();

//Load Model
let mixer;
let model;
const audioTag = document.getElementById('audio');
const clock = new THREE.Clock();

function onWindowResize() {
    if (window.innerWidth < 700) {
        camera.position.set(-8, 52, 50);
    } else {
        camera.position.set(-18, 52, 50);
    }

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}

window.addEventListener('resize', onWindowResize, false);

//Anime
function animate() {
    requestAnimationFrame(animate);

    if (model) {
        model.rotation.x = 0.3;
        mixer.update(clock.getDelta() / 7);
    }

    renderer.render(scene, camera);
}

animate();

//JS LOGIC
const soundIcon = document.getElementById('sound');

soundIcon.draggable = false;
soundIcon.addEventListener('click', handleSoundIconClick);

function handleSoundIconClick() {
    if (audioTag.muted) {
        audioTag.muted = false;
        soundIcon.src = 'utils/misc/volume-2.svg';
    } else {
        audioTag.muted = true;
        soundIcon.src = 'utils/misc/volume-x.svg';
    }
}

document.addEventListener('click', handleFirstClick, { once: true });

function handleFirstClick() {
    const loadingText = document.querySelector('#loading p');

    loadingText.innerText = 'Please wait, this may take a while...';

    loader.load(
        './utils/flash/scene.glb',
        (gltf) => {
            //Hide Loading Screen
            const loading = document.getElementById('loading');
            loading.classList.add('hide');

            model = gltf.scene;
            scene.add(model);

            mixer = new THREE.AnimationMixer(model);

            gltf.animations.forEach((clip) => {
                mixer.clipAction(clip).play();
            });

            //Play when model loads
            audioTag.play();
        },
        undefined,
        (error) => {
            console.error(error);
        },
    );
}

// Stuff

// "The Flash" (https://skfb.ly/6yHoA) by Polygon Artisan is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).
